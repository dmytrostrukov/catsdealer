# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'puma', '~> 4.1'
gem 'rails', '~> 6.0.2'

gem 'bootsnap', '>= 1.4.2', require: false
gem 'hamlit'
gem 'redis'

gem 'sidekiq'
gem 'sidekiq-scheduler'

group :development, :test do
  gem 'pry-rails'

  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'mock_redis'

  gem 'rspec-rails'
end

group :test do
  gem 'simplecov', require: false
end

group :development do
  gem 'rubocop', require: false

  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'

  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
