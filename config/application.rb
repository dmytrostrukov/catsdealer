# frozen_string_literal: true

require_relative 'boot'

require 'rails'

require 'active_model/railtie'
require 'active_job/railtie'

require 'action_controller/railtie'
require 'action_view/railtie'

require 'sprockets/railtie'

Bundler.require(*Rails.groups)

module Catsdealer
  class Application < Rails::Application
    config.load_defaults 6.0
  end
end
