# frozen_string_literal: true

require 'spec_helper'
require 'simplecov'

ENV['RAILS_ENV'] ||= 'test'

SimpleCov.start

require File.expand_path('../config/environment', __dir__)

abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
require 'support/factory_bot'

RSpec.configure do |config|
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
end
