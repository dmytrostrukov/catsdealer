# frozen_string_literal: true

FactoryBot.define do
  factory :shop_item, class: 'ShopItem' do
    name { Settings.cat_types.sample }
    price { rand(500..1000) }
    image { FFaker::Image.url }

    location { Settings.locations.sample }

    initialize_with { new(name: name, price: price, image: image, location: location) }
  end
end
