# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ShopItemSearch do
  let(:shop_item) { build(:shop_item) }
  let(:item) { shop_item.instance_values.symbolize_keys }

  let(:json_shop) { Settings.find_shop_by_format(format: :json) }
  let(:xml_shop)  { Settings.find_shop_by_format(format: :xml) }

  let(:mappings) { Shops::Serializers::ShopItemSerializer.instance.matchers[:xml].merge({ location: :location }) }

  before do
    Settings.redis.set(json_shop.url, [item].to_json)

    Settings.redis.set(
      xml_shop.url,
      {
        'cat' => [
          item.transform_keys(&mappings.method(:[]))
        ]
      }.to_xml(root: 'cats')
    )
  end

  describe '.call' do
    subject { described_class.call(location: item[:location], cat_type: item[:name]) }

    it 'search by shop items' do
      subject.each do |result_item|
        expect(result_item.instance_values).to eq shop_item.instance_values
      end
    end
  end
end
