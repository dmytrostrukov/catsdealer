# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Shops::ItemsFetcher do
  describe '.call' do
    let(:url) { Settings.shops.first.url }

    context 'when unsupported type of format' do
      let(:format) { :wrong }

      subject { described_class.call(url: url, format: format) }

      it {
        expect { subject }.to raise_error(NotImplementedError,
                                          "Unsupported type of adapter: #{format}")
      }
    end
  end
end
