# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Shops::Serializers::ShopItemSerializer do
  describe '.call' do
    let(:shop_items) { build_list(:shop_item, 5) }
    let(:objects) { shop_items.map(&:instance_values) }

    subject { described_class.call(objects: objects, format: format) }

    context 'when format is json' do
      let(:format) { :json }

      it 'unifies response format' do
        subject.each_with_index do |item, index|
          expect(item).to eq objects[index].symbolize_keys
        end
      end
    end

    context 'when format is xml' do
      let(:shop_item) { build(:shop_item) }
      let(:format) { :xml }

      let(:objects) do
        [
          {
            'title' => shop_item.name,
            'cost' => shop_item.price,
            'img' => shop_item.image,
            'location' => shop_item.location
          }
        ]
      end

      it 'unifies response format' do
        expect(subject.first).to eq shop_item.instance_values.symbolize_keys
      end
    end
  end
end
