# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Shops::Adapters::Base do
  let(:url) { 'http://random.com' }
  let(:instance) { described_class.new(url: url) }

  describe '#call' do
    subject { instance.call }

    it {
      expect { subject }.to raise_error(NotImplementedError,
                                        'Please follow the unified interface, add method #call')
    }
  end
end
