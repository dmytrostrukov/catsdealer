# frozen_string_literal: true

class Shop
  attr_reader :name, :url, :format

  def initialize(name:, url:, format:)
    @name = name
    @url = url

    @format = format
  end

  def items
    items_attributes = Shops::ItemsFetcher.call(url: url, format: format)

    items_attributes.map do |item_attributes|
      ShopItem.new(item_attributes.symbolize_keys)
    end
  end
end
