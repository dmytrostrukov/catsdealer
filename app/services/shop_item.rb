# frozen_string_literal: true

class ShopItem
  attr_reader :name, :price, :location, :image

  def initialize(name:, price:, location:, image:)
    @name = name

    @price = price
    @image = image

    @location = location
  end
end
