# frozen_string_literal: true

module ShopItemSearch
  def call(location:, cat_type:)
    Settings.shops.each_with_object([]) do |shop, shop_items|
      items = shop.items.select do |item|
        item.location == location && item.name == cat_type
      end

      shop_items.push(*items) if items.present?
      shop_items
    end
  end

  module_function :call
end
