# frozen_string_literal: true

module Shops
  module ItemsFetcher
    ADAPTERS = {
      json: Adapters::Json,
      xml: Adapters::Xml
    }.freeze

    def call(url:, format:, serializer: Serializers::ShopItemSerializer)
      adapter = adapter_by_format(format: format)

      serializer.call(
        objects: adapter.new(url: url).call,
        format: format
      )
    end

    module_function :call

    private

    def adapter_by_format(format:)
      raise NotImplementedError, "Unsupported type of adapter: #{format}" if ADAPTERS[format].nil?

      ADAPTERS[format]
    end

    module_function :adapter_by_format
  end
end
