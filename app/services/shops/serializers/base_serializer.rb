# frozen_string_literal: true

module Shops
  module Serializers
    class BaseSerializer
      include Singleton

      attr_accessor :current_format, :attributes
      attr_writer :matchers

      def matchers
        @matchers || {}
      end

      class << self
        def call(objects:, format:)
          objects.map do |object|
            serialize_object(object: object, format: format)
          end
        end

        def serialize_object(object:, format:)
          instance.attributes[format].each_with_object({}) do |attribute, result|
            result[attribute] = if attribute_with_matcher?(format: format, attribute: attribute)
                                  object[instance.matchers[format][attribute].to_s]
                                else
                                  object[attribute.to_s]
                                end

            result
          end
        end

        def format(value, &block)
          instance.current_format = value

          instance.attributes = {} if instance.attributes.nil?
          instance.attributes[value] = []

          block.call
        end

        def match(attribute, with:)
          instance.matchers = { instance.current_format => {} } if instance.matchers[instance.current_format].nil?
          instance.matchers[instance.current_format][attribute] = with
        end

        def attributes(*arguments)
          instance.attributes[instance.current_format].push(*arguments)
        end

        private

        def attribute_with_matcher?(format:, attribute:)
          instance.matchers[format].present? && instance.matchers[format][attribute.to_sym].present?
        end
      end
    end
  end
end
