# frozen_string_literal: true

module Shops
  module Serializers
    class ShopItemSerializer < BaseSerializer
      format :json do
        attributes :name, :price, :location, :image
      end

      format :xml do
        match :name,  with: :title
        match :price, with: :cost
        match :image, with: :img

        attributes :name, :price, :location, :image
      end
    end
  end
end
