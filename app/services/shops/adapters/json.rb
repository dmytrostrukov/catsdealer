# frozen_string_literal: true

module Shops
  module Adapters
    class Json < Base
      def call
        storage.set(uri.to_s, Net::HTTP.get(uri)) if storage.get(uri.to_s).nil?

        JSON.parse(storage.get(uri.to_s))
      end
    end
  end
end
