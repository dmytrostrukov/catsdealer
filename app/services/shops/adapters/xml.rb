# frozen_string_literal: true

module Shops
  module Adapters
    class Xml < Base
      def call
        if storage.get(uri.to_s).nil?
          storage.set(
            uri.to_s,
            Net::HTTP.get(uri)
          )
        end

        response = Hash.from_xml(storage.get(uri.to_s))
        response.dig('cats', 'cat')
      end
    end
  end
end
