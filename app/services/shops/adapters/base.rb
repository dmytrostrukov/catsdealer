# frozen_string_literal: true

require 'net/http'

module Shops
  module Adapters
    class Base
      def initialize(url:, storage: Settings.redis)
        @uri = URI(url)
        @storage = storage
      end

      def call
        raise NotImplementedError, 'Please follow the unified interface, add method #call'
      end

      private

      attr_reader :uri, :storage
    end
  end
end
