# frozen_string_literal: true

class Settings
  CONFIG_PATH = 'config/settings.yml'

  include Singleton

  attr_accessor :redis, :config, :shops, :cat_types, :locations

  class << self
    def redis
      instance.redis ||=
        if Rails.env.test?
          MockRedis.new
        else
          Redis.new
        end
    end

    def shops
      config.dig('shops').map do |name, attributes|
        Shop.new(name: name, url: attributes['url'], format: attributes['format'])
      end
    end

    def find_shop_by_format(format:)
      shops.find { |shop| shop.format == format }
    end

    def cat_types
      config.dig('data', 'cat_types')
    end

    def locations
      config.dig('data', 'locations')
    end

    private

    def config
      instance.config ||= YAML.load_file(CONFIG_PATH)
    end
  end
end
