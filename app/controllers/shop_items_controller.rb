# frozen_string_literal: true

class ShopItemsController < ApplicationController
  def index
    @shop_items = ShopItemSearch.call(
      location: params[:location],
      cat_type: params[:cat_type]
    )
  end

  def new; end
end
