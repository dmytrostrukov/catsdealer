# frozen_string_literal: true

require 'sidekiq-scheduler'

class SyncShopItemsWorker
  include Sidekiq::Worker

  def perform
    Settings.shops.each(&:items)
  end
end
